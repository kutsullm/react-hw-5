import { GET_PRODUCTS, TOGGLE_FAV_PRODUCT, ADD_ITEM_CART, DELETE_ITEM_CART, SHOW_MODAL, HIDE_MODAL, SET_CURRENT_ID, GET_CART, CLEAR_CART } from "../types";

export const getAllProducts = () => async (dispatch) => {
  const res = await fetch("data-base.json")
  const data = await res.json()
  const updatedProducts = updateData(data.productList)

  dispatch(getProducts(updatedProducts))
}
function updateData(products) {
  const cart = JSON.parse(localStorage.getItem("cart")) || []
  const fav = JSON.parse(localStorage.getItem("fav")) || []
  const newCart = cart.map((item) => item.id)

  const cartCheck = products.map((item) => {

    if (newCart.includes(item.id)) {
      const finded = cart.find(el => el.id === item.id)
      item.isInCart = true
      return { ...item, counter: finded.count }

    }

    return { ...item, count: 0 }
  })

  const favCheck = cartCheck.map((item) => {
    if (fav.includes(item.id)) {
      item.isInFav = true
      return item
    }
    return item
  })
  return favCheck
}

export const toggleFavoriteProducts = (id) => ({
  type: TOGGLE_FAV_PRODUCT,
  payload: id
})
export const getCart = (products) => ({
  type: GET_CART,
  payload: products
})

export const addToCart = (id, count = 1) => ({
  type: ADD_ITEM_CART,
  payload: {
    id,
    count
  }
})
export const setCurrentId = (id) => ({
  type: SET_CURRENT_ID,
  payload: id
})
export const deleteFromCart = (id) => ({
  type: DELETE_ITEM_CART,
  payload: id
})

export const clearCart = () => ({
  type: CLEAR_CART,
})

export const showModal = (id) => ({
  type: SHOW_MODAL,
  payload: id
})
export const hideModal = (id) => ({
  type: HIDE_MODAL,
  payload: id
})
export const getProducts = (data) => ({
  type: GET_PRODUCTS,
  payload: data
})


