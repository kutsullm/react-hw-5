import { ADD_ITEM_CART, DELETE_ITEM_CART, SET_CURRENT_ID, GET_CART, CLEAR_CART } from "../types";

const initialState = {
  currentId: null,
  items: localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [],
  cartProducts: {}
}

const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {

    case ADD_ITEM_CART: {
      const newItems = [...state.items];
      const { id } = payload;
      const inCart = newItems.find(({ id: currentId }) => id === currentId);

      if (inCart) {
        const findedIndex = newItems.findIndex((item) => item.id === id)
        const inCart = newItems.toSpliced(findedIndex, 1, payload)
        return {...state, items:inCart}
      } 
      if (!inCart) {
        return {...state, items:[...newItems, payload]}
      }
      return { ...state, items: newItems };
    }
    case SET_CURRENT_ID:
      const newState = { ...state, currentId: payload }

      return newState
    case GET_CART:
      const cartState = payload.reduce((acc, curr) => {
        acc[curr.id] = curr
        return acc
      }, {})
      return { ...state, cartProducts: cartState }
    case DELETE_ITEM_CART: {
      console.log(payload);
      const newItems = [...state.items];
      const inCartIndex = newItems.findIndex(({ id }) => payload === id);

      if (inCartIndex > -1) newItems.splice(inCartIndex, 1);
      localStorage.setItem('cart', JSON.stringify(newItems));

      return { ...state, items: newItems };
    }
    case CLEAR_CART:
      return {...state, items: []}

    default:
      return state;
  }
}

export default cartReducer;