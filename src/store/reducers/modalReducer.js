import { HIDE_MODAL, SHOW_MODAL } from "../types";

const initialState = {
  isShown: false,
  id:null
}

const modalReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOW_MODAL: 
      return { ...state, isShown: true,id:payload};

    case HIDE_MODAL: 
      return {...state, isShown: false, id:null};

    default:
      return state;
  }
}

export default modalReducer;