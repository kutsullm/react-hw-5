// import { saveFavouritesToLS } from "../../utils/handleLS";
import { GET_PRODUCTS, TOGGLE_FAV_PRODUCT } from "../types";

const initialState = {
}

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      const newState = action.payload.reduce((acc,curr)=>{
        acc[curr.id] = curr
        return acc
      },{})

      return newState

    case TOGGLE_FAV_PRODUCT:

      const favState = {...state}
      favState[action.payload].isInFav  = !favState[action.payload].isInFav


return favState

    default:
      return state;
  }
}

export default productsReducer