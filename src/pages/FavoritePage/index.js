import React from 'react'
import ProductList from '../../components/ProductList'

export const FavoritePage = () => {

  return (
    <>
      <ProductList id="fav" />
    </>
  )
}
