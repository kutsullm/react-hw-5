import { useDispatch } from "react-redux";
import Button from "../Button"
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';
import { StyledImg } from "./styled";
import PropTypes from "prop-types";
import { setCurrentId, toggleFavoriteProducts } from "../../store/action";
import { showModal, addToCart } from "../../store/action";

const Product = (props) => {
  const dispatch = useDispatch()

  const { title, url, article, price, description, id, isInFav } = props
  const handleClick = () => {
    dispatch(toggleFavoriteProducts(id))
    const storage = JSON.parse(localStorage.getItem("fav"))
    if (storage.includes(id)) {
      const updatedData = storage.filter((item) => item !== id)

      localStorage.setItem("fav", JSON.stringify(updatedData))
      return
    }
    if (!storage.includes(id)) {
      localStorage.setItem("fav", JSON.stringify([...storage, id]))
    }

  }
  return (
    <li>
      <p>{isInFav ? <AiFillHeart onClick={handleClick} /> : <AiOutlineHeart onClick={handleClick} />}</p>
      <h3>
        {title}
      </h3>
      <StyledImg src={url} alt={title} />
      <p>{price}</p>
      <p>{description}</p>
      <p>{article}</p>

      <Button text="add to card" backgroundColor="blue" onClick={() => {
        dispatch(showModal("addToCart"))
        dispatch(setCurrentId(id))
      }} />
    </li>
  )

}
Product.propTypes = {
  title: PropTypes.string,
  url: PropTypes.string,
  article: PropTypes.string,
  price: PropTypes.string,
  description: PropTypes.string,
  id: PropTypes.string,
  isInFav: PropTypes.bool
}
export default Product