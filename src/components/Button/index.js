
import { StyledButton } from "./styled";
import PropTypes from "prop-types";

const Button = (props) => {
    const { onClick, text, backgroundColor } = props
    return <StyledButton onClick={onClick} $bgColor={backgroundColor} >{text}</StyledButton>
}


Button.propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string,
    backgroundColor: PropTypes.string
}
export default Button;  