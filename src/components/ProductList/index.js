import { useSelector } from "react-redux";
import Product from "../Product/"
import PropTypes from "prop-types";
const ProductList = ({ id }) => {

   const products = useSelector(state => Object.values(state.products))
   let productList = null
   if(id==="all") {
      productList = products
   }
   if(id==="fav"){
      productList = products.filter(item=>item.isInFav)
   }

   return (
      <> <ul>
         {productList.map((product) => {
            return <Product key={product.id} {...product} />
         })}
      </ul>  </>
   )

}

ProductList.propTypes = {
   id: PropTypes.string,
}

export default ProductList