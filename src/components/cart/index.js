import React from 'react'
import { useDispatch } from "react-redux";
import Button from '../Button'
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';
import { StyledImg } from "./styled";
import { setCurrentId, toggleFavoriteProducts } from "../../store/action";
import { showModal } from "../../store/action";

export const Cart = ({ isInFav, title, description, url, counter, id }) => {
  const dispatch = useDispatch()
  const handleClick = () => {
    dispatch(toggleFavoriteProducts(id))
    const storage = JSON.parse(localStorage.getItem("fav"))
    if (storage.includes(id)) {
      const updatedData = storage.filter((item) => item !== id)

      localStorage.setItem("fav", JSON.stringify(updatedData))
      return
    }
    if (!storage.includes(id)) {
      localStorage.setItem("fav", JSON.stringify([...storage, id]))
    }

  }
  return (
    <li>
      <p>{isInFav ? <AiFillHeart onClick={handleClick} /> : <AiOutlineHeart onClick={handleClick} />}</p>
      <h4>Title: {title}</h4><StyledImg src={url} />
      <p>Count in Cart: {counter}</p>
      <p>Description: {description}</p>
      <Button backgroundColor="alice-blue" text="delete" onClick={() => {
        dispatch(showModal("removeFromCart"))
        dispatch(setCurrentId(id))
      }} /></li>
  )
}
