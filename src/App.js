import { Route, Routes } from "react-router-dom"
import { HomePage } from "./pages/HomePage";
import { FavoritePage } from "./pages/FavoritePage";
import { CartPage } from "./pages/CartPage";
import { Header } from "./components/Header";
import { useState, useEffect } from "react";
import { getAllProducts } from "./store/action";
import { useDispatch, useSelector } from "react-redux"
import { getCart } from "./store/action"
import Modal from "./components/Modal";

function App() {
  const isOpenModal = useSelector(state => state.modal.isShown)
  const products = useSelector(state => Object.values(state.products))

  const dispatch = useDispatch()
  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem("cart"))
    if (!cart) {
      localStorage.setItem("cart", JSON.stringify([]))
    }
    if (products) {
      const filtered = products.filter(item => item.isInCart)
      dispatch(getCart(filtered))
    }
  }, [dispatch])

  useEffect(() => {
    const fav = JSON.parse(localStorage.getItem("fav"))
    if (!fav) {
      localStorage.setItem("fav", JSON.stringify([]))
    }
  }, [])

  useEffect(() => {
    dispatch(getAllProducts())
  }, [dispatch])

  return (
    <div className="">
      <Header />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/cart" element={<CartPage />} />
        <Route path="/favorite" element={<FavoritePage />} />
      </Routes>
      {isOpenModal && <Modal />}
    </div>
  );
}

export default App;
